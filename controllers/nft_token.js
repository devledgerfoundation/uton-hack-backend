const NftToken = require('../models/NftToken')
const axios = require("express/lib/request");
const User = require('../models/User');
const http = require("express/lib/request");
const request = require('request-promise');
const ObjectId = require('mongoose').Types.ObjectId;

exports.create = async (req, res) => {
    console.log('req.body: ', req.body)
    const {name, model_id, user_id, description, image_uri, token_id} = req.body

    try {
        const createdNftToken = await NftToken.create({
            name, model_id, user_id, description, image_uri, token_id
        })

        res.json(createdNftToken)
    } catch (e) {
        console.log("Couldn't create NFT token object")
        console.log(e)
        return res.json({
            error: true,
            message: e
        })
    }
}

exports.getByModelId = async (req, res) => {
    try {
        const findNftToken = await NftToken.findOne({
            model_id: req.params.modelId
        })

        if (!findNftToken) {
            return res.status(400).json({success: false, message: "No NFT Token with such model_id"});
        }

        res.json(findNftToken)
    } catch (e) {
        console.log("Couldn't retrieve NFT Toekn by model_id")
        return res.json({
            error: true,
            message: e
        })
    }
}

exports.getSmartContractInfo = async (req, res) => {
    const findNftToken = await NftToken.findOne({
        model_id: req.params.modelId
    })

    if (!findNftToken) {
        return res.status(400).json({success: false, message: "No NFT Token with such model_id"});
    }

    return res.json({
        image: findNftToken.image_uri,
        description: findNftToken.description,
        name: findNftToken.name
    })
}


exports.getAllNftsByUserId = async (req, res) => {
    const findUser = await User.findById(req.params.userId).populate('models');

    if (!findUser) {
        return res.status(400).json({success: false, message: "No user with such id"});
    }

    const nfts = NftToken.find({"user_id": new ObjectId(findUser._id)})

    res.json(nfts)
}

exports.getAllNftsByUserEmail = async (req, res) => {
    const findUser = await User.findOne({email: req.params.userEmail}).populate('models');
    if (!findUser) {
        return res.status(400).json({success: false, message: "No user with such email"});
    }

    const nfts = await NftToken.find({"user_id": new ObjectId(findUser._id)})

    console.log(nfts)

    await Promise.all(nfts)

    res.json(nfts)
}
