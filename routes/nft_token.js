const express = require('express');
const nft_token = require('../controllers/nft_token')
const router = express.Router();
const {auth} = require('../middleware/auth');

router.post('/create', nft_token.create)

router.get('/getByModelId/:modelId', nft_token.getByModelId)

router.get('/getAllNftsByUserId/:userId', nft_token.getAllNftsByUserId);

router.get('/getAllNftsByUserEmail/:userEmail', nft_token.getAllNftsByUserEmail);

module.exports = router;