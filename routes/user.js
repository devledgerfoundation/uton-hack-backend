const express = require('express');
const User = require('../models/User');
const router = express.Router();
const {auth} = require('../middleware/auth');
const user = require('../controllers/user');

router.post('/register', user.register);

router.post('/login', user.login);

router.post('/logout', user.logout);

router.get('/refresh', user.refresh)

router.get('/getAllUsers', user.getAllUsers)

router.post('/addUser', user.addUser)

router.get('/getUserById/:userId', user.getUserById)

router.get('/getUserByEmail/:email', user.getUserByEmail)

router.delete('/deleteUser/:userId', user.deleteUser)

router.post('/updateUser/:userId', user.updateUser)

router.post('/updateUserModel/:userId', user.updateUserModel)

router.post('/updateUserTokenAmount/:userId', user.updateUserTokenAmount)

router.post('/updateUserResources/:userId', user.updateUserResources)

router.post('/profile', user.profile);

module.exports = router;