const express = require('express');
const model = require('../controllers/model')
const router = express.Router();
const {auth} = require('../middleware/auth');

router.get('/getAll', model.getAll);

router.get('/getById/:modelId', model.getById);

router.get('/getByUserId/:userId', model.getByUserId)

router.get('/getModelIdByUserId/:userId', model.getModelIdByUserId)

router.get('/getModelByUserId/:userId', model.getModelByUserId)

router.post('/create/:userId', model.createWithBaseImage)

router.post('/uploadTokenImage/:modelId', model.uploadNftTokenImage)

router.post('/updateModelPrintedStatus/:modelId', model.uploadModelPrintedStatus)

router.post('/update', model.update)

module.exports = router;