const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const Schema = mongoose.Schema;
const salt = 10;

const Token = mongoose.Schema({
    user: {type: Schema.Types.ObjectId, ref: 'User', required: true},
    refreshToken: {type: String, required: true},
}, {timestamps: {createdAt: 'created_at'}});

module.exports = mongoose.model('Token', Token);