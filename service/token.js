const jwt = require('jsonwebtoken')
const Token = require('../models/Token')
exports.generateTokens = (payload) => {
    const accessToken = jwt.sign(payload, process.env.JWT_ACCESS_SECRET, {expiresIn: '24h'})
    const refreshToken = jwt.sign(payload, process.env.JWT_REFRESH_SECRET, {expiresIn: '30d'})

    return {
        accessToken,
        refreshToken
    }
}

exports.saveToken = async (userId, refreshToken) => {
    const tokenData = await Token.findOne({user: userId})

    if (tokenData) {
        tokenData.refreshToken = refreshToken;
        return tokenData.save();
    }
    return await Token.create({user: userId, refreshToken});
}

exports.removeToken = async (refreshToken) => {
    return Token.deleteOne({refreshToken: refreshToken});
}

exports.validateAccessToken = async (token) => {
    try {
        return jwt.verify(token, process.env.JWT_ACCESS_SECRET);
    } catch (err) {
        console.log(err);
        return null;
    }
}

exports.validateRefreshToken = async (token) => {
    try {
        return jwt.verify(token, process.env.JWT_REFRESH_SECRET);
    } catch (err) {
        console.log(err);
        return null;
    }
}

exports.findRefreshToken = async (token) => {
    return Token.findOne({refreshToken: token});
}