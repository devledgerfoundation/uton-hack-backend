const User = require('../models/User')
const bcrypt = require('bcrypt')
const tokenService = require('./token')
const likelib = require("@likelib/core");

exports.register = async (userBody) => {
    if (userBody.password !== userBody.password_repeat) {
        throw new Error("Passwords are not the same")
    }

    const candidate = await User.findOne({email: userBody.email});
    if (candidate) {
        throw new Error(`User with such email ${userBody.email} already exists`)
    }

    const hashPassword = await bcrypt.hash(userBody.password, 10)

    const newUser = new User(userBody);
    newUser.password = hashPassword
    newUser.password_repeat = hashPassword

    const account = likelib.Account.createAccount()
    console.log(account.getAddress())

    newUser.walletAddress = account.getAddress()
    const user = await User.create(newUser);

    const tokens = tokenService.generateTokens({
        email: user.email,
        username: user.username
    })

    await tokenService.saveToken(user._id, tokens.refreshToken)

    return {
        ...tokens,
        user: user,
    }
}

exports.login = async (email, password) => {
    const user = await User.findOne({email})
    if (!user) {
        throw new Error(`User with such email ${userBody.email} doesn\'t exists`)
    }

    const passwordMatch = await bcrypt.compare(password, String(user.password))
    if (!passwordMatch) {
        throw new Error('Invalid Credentials')
    }

    const tokens = tokenService.generateTokens({
        email: user.email,
        username: user.username
    })

    await tokenService.saveToken(user._id, tokens.refreshToken)

    return {
        ...tokens,
        user: user,
    }
}

exports.logout = async (refreshToken) => {
    return await tokenService.removeToken(refreshToken);
}

exports.refresh = async (refreshToken) => {
    if (!refreshToken) {
        throw Error('User is not authorized');
    }

    const user = tokenService.validateRefreshToken(refreshToken);
    const tokenDB = await tokenService.findRefreshToken(refreshToken);
    if (!user || !tokenDB) {
        throw Error('User is not authorized');
    }

    const tokens = tokenService.generateTokens({
        email: user.email,
        username: user.username
    })

    await tokenService.saveToken(user._id, tokens.refreshToken)

    return {
        ...tokens,
        user: user,
    }
}